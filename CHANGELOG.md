# [1.5.0](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.3...1.5.0) (2022-05-01)


### Features

* configurable tracking image ([3409cb7](https://gitlab.com/to-be-continuous/mkdocs/commit/3409cb796725f988dc7f4507d8f4d051372e3e76))

## [1.4.3](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.2...1.4.3) (2021-11-18)


### Bug Fixes

* **requirements:** multiple requirements in var ([e1e01c3](https://gitlab.com/to-be-continuous/mkdocs/commit/e1e01c314716de1735023558b57d6ba8aef9ab53))

## [1.4.2](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.1...1.4.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([eb8f82d](https://gitlab.com/to-be-continuous/mkdocs/commit/eb8f82dc374b4deee1941e01f5d51be1f7e0b478))

## [1.4.1](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.0...1.4.1) (2021-09-08)

### Bug Fixes

* change variable behaviour ([e4eea17](https://gitlab.com/to-be-continuous/mkdocs/commit/e4eea17816f3081c5b0c68384d857dbd471d99ce))

## [1.4.0](https://gitlab.com/to-be-continuous/mkdocs/compare/1.3.0...1.4.0) (2021-06-10)

### Features

* move group ([1b42366](https://gitlab.com/to-be-continuous/mkdocs/commit/1b42366c3d2479b25ba22d99d6c2b2cacd136be2))

## [1.3.0](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.2.0...1.3.0) (2021-05-20)

### Features

* **pages:** declare the environment for pages job ([3f27b3c](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/3f27b3cf1fb6d8f7812da56141124ff84046171f))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.1.1...1.2.0) (2021-05-18)

### Features

* add scoped variables support ([1b6db00](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/1b6db00827b6f7829ce7c3ac2af3ceaa07701a5e))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.1.0...1.1.1) (2021-05-17)

### Bug Fixes

* wrong cache scope ([8a39efa](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/8a39efad2ee173e1e0b95ba68021fde0bb04f40b))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.0.0...1.1.0) (2021-05-07)

### Bug Fixes

* **pages:** handle mkdocs output folder to already be 'public' ([2e12aa1](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/2e12aa11478ed09318745170561f250b7e40d92d))

### Features

* **stage:** moving pages job to production stage ([2d90389](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/2d9038958a324d189993135c77af9300673d7530))

## 1.0.0 (2021-05-06)

### Features

* initial release ([1827646](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/18276469c82a8764d5f0b89251eaa4b2f8e976d5))
