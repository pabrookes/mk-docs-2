# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms 
# of the GNU Lesser General Public License as published by the Free Software Foundation; 
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this 
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules
workflow:
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: always

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: "$CI_REGISTRY/to-be-continuous/tools/tracking:master"

  MKD_WORKSPACE_DIR: .
  MKD_IMAGE: polinux/mkdocs:latest
  MKD_REQUIREMENTS: "mkdocs"
  MKD_REQUIREMENTS_FILE: "requirements.txt"
  MKD_SITE_DIR: "site"
  MKD_PREBUILD_SCRIPT: "mkdocs-pre-build.sh"

# ==================================================
# Stages definition
# ==================================================
stages:
  - build

.mkdocs-scripts: &mkdocs-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function prepare_mkdocs() {
    cd "${MKD_WORKSPACE_DIR}"

    if [[ -f "${MKD_REQUIREMENTS_FILE}" ]]; then
      log_info "installing requirements from file ${MKD_REQUIREMENTS_FILE}"
      # shellcheck disable=SC2086
      pip install -r "${MKD_REQUIREMENTS_FILE}" ${PIP_OPTS}
    elif [[ "${MKD_REQUIREMENTS}" ]]; then
      log_info "installing requirements from variable 'MKD_REQUIREMENTS': ${MKD_REQUIREMENTS}"
      # shellcheck disable=SC2086
      pip install ${MKD_REQUIREMENTS} ${PIP_OPTS}
    else
      log_info "no requirement defined"
    fi

    if [[ -f "${MKD_PREBUILD_SCRIPT}" ]]; then
      log_info "--- \\e[32mpre-build hook\\e[0m (\\e[33;1m${MKD_PREBUILD_SCRIPT}\\e[0m) found: execute"
      chmod +x "${MKD_PREBUILD_SCRIPT}"
      "./${MKD_PREBUILD_SCRIPT}"
    fi
  }

  function install_ca_certs() {
    certs="$1"

    if [[ -z "${certs}" ]]
    then
      return
    fi

    # List of typical bundles
    bundles=''
    bundles="${bundles} /etc/ssl/certs/ca-certificates.crt"                 # Debian/Ubuntu/Gentoo etc.
    bundles="${bundles} /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem"  # CentOS/RHEL 7
    bundles="${bundles} /etc/ssl/cert.pem"                                  # Alpine Linux
    bundles="${bundles} /etc/pki/tls/certs/ca-bundle.crt"                   # Fedora/RHEL 6
    bundles="${bundles} /etc/ssl/ca-bundle.pem"                             # OpenSUSE
    bundles="${bundles} /etc/pki/tls/cacert.pem"                            # OpenELEC
    bundles="${bundles} /kaniko/ssl/certs/ca-certificates.crt"              # Kaniko

    # Try to find the right bundle to update it with custom CA certificates
    for bundle in ${bundles}
    do
      # Check if bundle exists
      if [[ ! -f "${bundle}" ]]
      then
        continue
      fi

      # Import certificates in bundle
      echo "${certs}" | tr -d '\r' >> "${bundle}"

      log_info "Custom CA certificates imported in \\e[33;1m${bundle}\\e[0m"
      imported=1
      break
    done

    if [[ ${imported:-0} -eq 0 ]]
    then
      log_warn "Could not import custom CA certificates !"
    else
      # $REQUESTS_CA_BUNDLE required by Python Requests, used by PIP
      export REQUESTS_CA_BUNDLE="${bundle}"
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  unscope_variables

  # ENDSCRIPT

mkdocs:
  stage: build
  image: ${MKD_IMAGE}
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "mkdocs", "1.5.0" ]
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    key: "$CI_COMMIT_REF_SLUG-mkdocs"
    paths:
      - ${PIP_CACHE_DIR}
  before_script:
    - *mkdocs-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - prepare_mkdocs
  script:
    - mkdocs build ${MKD_BUILD_ARGS}
  artifacts:
    name: "mkdocs build from $CI_COMMIT_REF_SLUG"
    paths:
      - ${MKD_WORKSPACE_DIR}/${MKD_SITE_DIR}
    expire_in: 1 day

