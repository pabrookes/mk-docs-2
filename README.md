# GitLab CI template for MkDocs

This project implements a generic GitLab CI template for [MkDocs](https://www.mkdocs.org/).

It provides several features, usable in different modes (by configuration).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/mkdocs'
    ref: '1.5.0'
    file: '/templates/gitlab-ci-mkdocs.yml'
```

## Global configuration

The MkDocs template uses some global configuration used throughout all jobs.

| Name                    | description                            | default value     |
| ----------------------- | -------------------------------------- | ----------------- |
| `MKD_IMAGE`             | The Docker image used to run MkDocs       | `polinux/mkdocs:latest` |
| `MKD_WORKSPACE_DIR`     | MkDocs sources directory | `.` |
| `MKD_REQUIREMENTS_FILE` | Requirements file. If the file is not found in the repository, requirements are read from the `MKD_REQUIREMENTS` variable | `requirements.txt` |
| `MKD_REQUIREMENTS`      | Space separated requirements (ignored if a requirement file is found) | `mkdocs` |
| `MKD_SITE_DIR`          | MkDocs generated site directory (relative to `$MKD_WORKSPACE_DIR`), the path will be declared as artifact | `site` |
| `MKD_PREBUILD_SCRIPT`      | Pre-build hook script | `mkdocs-pre-build.sh` |
| `PIP_INDEX_URL`      | Python repository url | _none_ |
| `PIP_OPTS`      | pip extra [options](https://pip.pypa.io/en/stable/reference/pip/#general-options) | _none_ |

## Jobs

### `mkdocs` job

This job performs MkDocs **build**. It uses the following variable:

| Name                    | description                                       | default value     |
| ----------------------- | ------------------------------------------------- | ----------------- |
| `MKD_BUILD_ARGS`        | Arguments used by the build job                   | _none_ |

:warning: The built documentation is generated in the `${MKD_WORKSPACE_DIR}/${MKD_SITE_DIR}` folder.
Be sure to update `MKD_SITE_DIR` if you change the output directory (via the `MKD_BUILD_ARGS` variable or the `mkdocs.yml` file).

## Publication

:warning: this template is not a deployment template and it only builds a MkDocs project.

You might deploy the generated site using a [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) job (there is [a variant for that](#gitlab-pages-variant)) or any other method you see fit.

## Variants

### GitLab Pages variant

Basically it copies the content of the mkdocs generated site folder (`site` by default) to the `public` folder which is published by [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/#how-it-works).

If you wish to use it, add the following to your `gitlab-ci.yml`:

```yaml
include:
  # main template
  - project: 'to-be-continuous/mkdocs'
    ref: '1.5.0'
    file: '/templates/gitlab-ci-mkdocs.yml'
  # GitLab pages variant
  - project: 'to-be-continuous/mkdocs'
    ref: '1.5.0'
    file: '/templates/gitlab-ci-mkdocs-pages.yml'
```
